package experiments;

import java.util.Random;

public class Uloha8_03
{
    public static void main(String[] args)
    {
        while(true) // jiná varianta jak udělat něco jako cyklus v úloze 2
        {
            int result = simulation();
            System.out.println(result);
            if(result >= 50)
            {
                break;
            }
        }
    }

    // Tato metoda simuluje házení dvěma kostkami (ty se sčítají)
    // a informuje, kolikrát se hodilo než se desetkrát vyskytl výsledek 7
    public static int simulation()
    {
        Random random = new Random();
        int hozeno = 0;
        int totalniSoucet = 0;
        while (totalniSoucet < 100)
        {
            hozeno++;
            int a = random.nextInt(6)+1;
            int b = random.nextInt(6)+1;
            int soucet = a+b;
            totalniSoucet += soucet;
        }
        return hozeno;
    }
}
