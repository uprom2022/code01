package experiments;

import java.util.Random;

public class Uloha8_04
{
    public static void main(String[] args)
    {
        System.out.println(sqrt(62200));
        System.out.println(sqrt(622000));
        System.out.println(sqrt(6220000));
        System.out.println(sqrt(62200000));
    }

    public static double sqrt(double x)
    {
        double left = 0;
        double right = x;
        while (true)
        {
            double middle = left + (right - left)/ 2;  //nebo (left + right)/2;
            if(Math.abs(middle*middle - x) < 0.001)
            {
                return middle;
            }
            else if(middle*middle < x)
            {
                left = middle; // zvýším left - vyberu pravou půlku
            }
            else
            {
                right = middle; // snížím right - vyberu levou půlku
            }
        }
    }
}
