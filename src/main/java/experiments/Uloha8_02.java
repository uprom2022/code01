package experiments;

import java.io.Console;
import java.util.Random;
import java.util.Scanner;

public class Uloha8_02
{
    public static void main(String[] args)
    {
        int result=Integer.MAX_VALUE; // Šlo by to řešit třeba pomocí break
        while(result>=13) {
            result = simulation();
            System.out.println(result);
        }
    }

    // Tato metoda simuluje házení dvěma kostkami (ty se sčítají)
    // a informuje, kolikrát se hodilo než se desetkrát vyskytl výsledek 7
    public static int simulation()
    {
        Random random = new Random();

        int hozeno7=0;
        int hozeno = 0;
        while (hozeno7<10)
        {
            hozeno++;
            int a = random.nextInt(6)+1;
            int b = random.nextInt(6)+1;
            int soucet = a+b;
            if(soucet == 7)
            {
                hozeno7++;
            }
        }
        return hozeno;
    }
}
