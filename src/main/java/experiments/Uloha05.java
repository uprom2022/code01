package experiments;

public class Uloha05
{
    public static void main(String[] args)
    {
        int[] test = range(5666);
        for (int i=0;i<test.length;i++)
        {
            System.out.println(test[i]);
        }
    }

    public static int[] range(int count)
    {
        // Zpusoby vytvoření pole:
        //int[] pole1 = {684,687,687,89,33,687}; // pole s daným obsahem
        int[] pole2 = new int[count]; // pole určitého počtu nul

        for (int i=0; i<pole2.length; i++)
        {
            pole2[i] = i;
        }

        return pole2;
    }
}
