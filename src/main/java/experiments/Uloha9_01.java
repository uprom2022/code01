package experiments;

public class Uloha9_01
{
    public static void main(String[] args)
    {
        String[] students = {"Jan Novák", "Jiří Procházka",
        "Antonín Čermák", "Karel Příhoda"};
        int index = indexOf(students,"Antonín Čermák");
        System.out.println(index);
    }

    public static int indexOf(String[] values, String toFind)
    {
        int index=0;
        while (!values[index].equals(toFind))
        {
            index++;
            if(index >= values.length)
            {
                return -1;
            }
        }
        return index;

//        int result = -1;
//        for (int i=0;i< values.length;i++)
//        {
//            if(values[i].equals(toFind))
//            {
//                result = i;
//                break;
//            }
//        }
//
//        return result;
    }
}
