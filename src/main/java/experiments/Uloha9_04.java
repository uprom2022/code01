package experiments;

import java.util.ArrayList;

public class Uloha9_04
{
    public static void main(String[] args)
    {
        Uloha9_04_List list = new Uloha9_04_List();
        //ArrayList<String> list2 = new ArrayList<>();

        for (int i=0; i< 10;i++)
        {
            list.add("Ahoj"+i);
        }

        list.insert(11,"Hello"); // nestane se NIC

        list.insert(5,"Hello");
        list.insert(6,"Hello");

        list.delete(6);
        list.delete(5);

        System.out.println(list.toString());

//        String[] messages= {};
//        int count = 0;
//        add(messages, "Ahoj", count);
//        count++;
//        add(messages, "Ahoj2", count);
//        count++;
//        add(messages, "Ahoj3", count);
//        count++;
    }
}
