package experiments;

public class Uloha9_02
{
    public static void main(String[] args)
    {
        String[] students = {"Jan Novák", "Jiří Procházka",
        "Antonín Čermák", "Karel Příhoda"};
        int index = findDuplicate(students);
        System.out.println(index);
    }

    public static int findDuplicate(String[] values)
    {
       for (int i=0; i<values.length-1; i++)
       {
           if(values[i].equals(values[i+1]))
           {
               return i;
           }
       }

       return -1;
    }
}
