package experiments;

public class Uloha04
{
    public static void main(String[] args)
    {
        int[] vstupniPole = {468, 687, 7, 778};
        int limit = 500;
        System.out.println(sumBelowLimit(vstupniPole, limit));
        System.out.println(limit);
    }

    public static int sumBelowLimit(int[] vstupniPole, int limit)
    {
        int sum = 0;
        for(int i=0; i<vstupniPole.length; i++)
        {
            if(vstupniPole[i] < limit)
            {
                sum += vstupniPole[i];
            }
        }
        limit = 5000;
        return sum;
    }
}
