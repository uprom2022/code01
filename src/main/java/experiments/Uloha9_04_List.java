package experiments;

public class Uloha9_04_List
{
    String[] values = new String[1000];
    int count = 0;

    public void add(String newValue)
    {
        values[count] = newValue;
        count++;
    }

    public void copyFrom(String[] source, int sourceIndex, int count, int destinationIndex)
    { // pozor na count (parametr metody) a this.count (atribut objektu)

        if(destinationIndex > this.count)
        {
            // destinationIndex je tak velký, že by vznikla mezera mezi původními a novými daty
            return;
        }

        for(int i=0; i<count; i++)
        {
            values[destinationIndex + i] = source[sourceIndex + i];
        }

        if(destinationIndex + count > this.count)
        {
            // pokud nejvyšší pozice v nových datech je vyšší než this.count-1, musí se this.count aktualizovat
            this.count = destinationIndex + count;
        }
    }

    public void copyTo(String[] destination, int sourceIndex, int count, int destinationIndex)
    {
        if(sourceIndex + count > this.count)
        {
            // interval, který čteme ze seznamu, sahá za okraj seznamu
            return;
        }

        for(int i=0; i<count; i++)
        {
            destination[destinationIndex + i] = values[sourceIndex + i];
        }
    }

    public void insert(int index, String newValue)
    {
        // Pokud je index moc velký, skončíme
        if(index > count)
        {
            return;
        }
        // Posuň prvky doprava
        // (nejdřív poslední prvek, nakonec prvek na pozici index)
        // Tím se pozice index uvolní
        for (int i=count-1; i>=index; i--)
        {
            values[i+1] = values[i];
        }
        // Zapiš nový prvek na pozici index
        values[index] = newValue;
        // Zvyš count
        count++;
    }

    public void delete(int index)
    {
        for (int i=index; i <= count-2; i++)
        {
            values[i] = values[i+1];
        }
        // Bezpečnostní opatření:
        values[count] = null;

        count--;
    }

    public String toString()
    {
        String result = "";

        for (int i=0; i<count; i++)
        {
            result = result + ", "+values[i];
        }

        return result;
    }
}
