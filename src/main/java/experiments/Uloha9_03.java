package experiments;

public class Uloha9_03
{
    public static void main(String[] args)
    {
        String s = "AHOJ";
        ModifyFirst2(s);
        System.out.println(s); //?HOJ

        String[] students = {"Jan Novák", "Jiří Procházka",
        "Antonín Čermák", "Karel Příhoda"};
        swap(students,2,3);

        double x = 5;
        double y = 55;

        swap2(x,y);

        System.out.println(x); //5
        System.out.println(y); //55
    }

    public static void swap(String[] values, int index1, int index2)
    {
        String temp = values[index2];
        values[index2] = values[index1];
        values[index1] = temp;
    }

    // Přehoď hodnoty dvou proměnných
    public static void swap2(double number1, double number2)
    {
        double temp = number1;
        number1 = number2;
        number2 = temp;
    }

// uprav první pozici v poli charů na '?'
    public static void ModifyFirst1(char [] chars)
    {
        chars[0] = '?';
    }

    // uprav první pozici ve stringu na '?'
    public static void ModifyFirst2(String string)
    {
        //string[0] = '?';
    }
}
